import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @Input() sideNavStatus: boolean=false;
  list = [
    {
      number: '1',
      name: 'Home',
      icon: 'fa-solid fa-house',
    },
    {
      number: '2',
      name: 'Recommendations',
      icon: 'fa-solid fa-user',
    },
    {
      number: '3',
      name: 'Favorites',
      icon: 'fa-solid fa-bookmark',
    },
    {
      number: '4',
      name: 'About Us',
      icon: 'fa-solid fa-circle-info',
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
